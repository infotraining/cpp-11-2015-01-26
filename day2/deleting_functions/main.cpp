#include <iostream>

using namespace std;

bool is_odd(int n)
{
    return n%2;
}

bool is_odd(double) = delete;
bool is_odd(char) = delete;

int main()
{
    cout << "Hello World!" << endl;
    cout << "is 2 odd " << is_odd(2) << endl;
    cout << "is 3 odd " << is_odd(3) << endl;
    cout << "is 'a' odd " << is_odd('a') << endl;
    cout << "is 12.34 odd " << is_odd(12.34) << endl;
    return 0;
}

