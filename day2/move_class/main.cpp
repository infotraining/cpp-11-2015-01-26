#include <iostream>

using namespace std;

class Array
{
    int* arr_  = nullptr;
    size_t size_ {0};
public:
    Array(size_t size, int initial_val) : size_(size)
    {
        if (size > 0)
        {
            arr_ = new int[size_];
            for (size_t i = 0 ; i < size_ ; ++i)
                arr_[i] = initial_val;
        }
    }

    Array(const Array& rop) = delete;
    Array operator=(const Array& rop) = delete;

    // Array(Array&& rop) = default; <- does not work...

    Array(Array&& rop) // move constructor
    {
        cout << "Move constructor" << endl;
        size_ = rop.size_;
        swap(arr_, rop.arr_);
    }

    Array& operator=(Array&& rop) // move assignement
    {
        cout << "Move asignement" << endl;
        size_ = rop.size_;
        swap(arr_, rop.arr_);
        return *this;
    }

    Array(size_t size) : Array(size, 0) {}
    ~Array() { delete[] arr_; }

    void print()
    {
        cout << "[ ";
        for (size_t i = 0 ; i < size_ ; ++i)
        {
            cout << arr_[i] << ", ";
        }
        cout << "]" << endl;
    }
};

Array generate_array()
{
    Array arr(5, 42);
    return arr;
}

int main()
{
    cout << "Hello Array" << endl;
    Array arr{10, -1};
    //Array arr2 = arr; <- deleted copy constructor
    Array arr2(move(arr));
    Array arr3 = generate_array();
    Shout shout;
    arr2.print();
    arr3.print();
    arr = Array(10,-2);
    arr.print();
    return 0;
}

