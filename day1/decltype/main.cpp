#include <iostream>

using namespace std;

//auto add(int a, int b) -> int
//{
//    return a + b;
//}

template <typename A, typename B>
auto add(A a, B b) -> decltype(a+b) // C++11
{
    return a+b;
}

template <typename A, typename B>
decltype(auto) add14(A a, B b)  // C++14
{
    return a+b;
}

int main()
{
    cout << "Hello decltype!" << endl;

    int a = 10;

    decltype(a+10.10) b = a;

    auto result = add14(10.0f, 8.3);

    cout << result << endl;

    return 0;
}


