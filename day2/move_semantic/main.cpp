#include <iostream>
#include <string>

using namespace std;

int my_fun(int a)  /* lvalues all way down */
{
    cout << "a = " << a << endl;
    int b = a * 2;
    return b;
}

//string foo(string arg)
//{
//    cout << "foo(string arg)" << endl;
//    return arg + " foo";
//}

string foo(const string& arg)
{
    cout << "const string& arg" << endl;
    return arg + " foo const";
}

//string foo(string& arg)
//{
//    cout << "string& arg" << endl;
//    arg += " foo";
//    return arg;
//} // not so good

string foo(string&& arg)
{
    cout << "string&& arg" << endl;
    arg += " foo";
    return arg;
}

int main()
{
    cout << "Hello World!" << endl;
    int z{4};
    int x = my_fun(z);
    string str = "Leszek";
    string res = foo(str);
    string&& res_ref = foo(move(str));
    //string& refres = foo(str);
    cout << "Res ref = " << res_ref << endl;
    cout << "Str = " << str << endl;
    return 0;
}

