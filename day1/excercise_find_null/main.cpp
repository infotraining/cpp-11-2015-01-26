#include <iostream>
#include <vector>

using namespace std;

template <typename Container>
auto find_null(const Container& cont) -> decltype(begin(cont))
{
    auto it = begin(cont);
    for (; it != end(cont) ; ++it)
    {
        if (!*it) break;
    }
    return it;
}

int main()
{
    cout << "Find null element!" << endl;

    vector<int*> vec = { new int(10), new int(4), nullptr, new int(5)};

    int* tab[] = { new int(10), new int(4), nullptr, new int(5)};

    // napisac funkcje ktora zwoci iterator na pierwszy nullptr, lub iterator na koniec


    for (auto& el : vec)
    {
        if (el)
            cout << *el << endl;
    }

    cout << "--------------------" << endl;

    for (auto it = begin(tab) ; it != find_null(tab) ; ++it)
    {
        cout << **it << endl;
    }

    return 0;
}

