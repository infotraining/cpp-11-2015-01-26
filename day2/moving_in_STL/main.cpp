#include <iostream>
#include <vector>

using namespace std;

class Shout
{
    int id_;
public:
    Shout(int id) : id_(id)
    {
        cout << "Ctor " << id_ << endl;
    }

    ~Shout()
    {
        cout << "Dtor " << id_ << endl;
    }

    Shout(const Shout& rop)
    {
        id_ = rop.id_;
        cout << "Copy Ctor " << rop.id_ << endl;
    }

    Shout operator=(const Shout& rop)
    {
        id_ = rop.id_;
        cout << "Copy Assignement " << rop.id_ << endl;
    }

    Shout(Shout&& rop) noexcept(noexcept(swap<int, int>)) // this allows to use it in vector reallocation
    {
        swap(id_, rop.id_);
        cout << "Move Ctor " << rop.id_ << endl;
    }

    Shout& operator=(Shout&& rop)  noexcept
    {
        id_ = rop.id_;
        cout << "Move Assignement " << rop.id_ << endl;
    }
};

int main()
{
    cout << "Hello Shout!" << endl;

    vector<Shout> vec;
    //vec.reserve(5);
    for(int i = 0 ; i < 5 ; ++i)
    {
        vec.push_back(Shout{i});
    }

    return 0;
}

