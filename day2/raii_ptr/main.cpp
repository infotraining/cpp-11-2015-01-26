#include <iostream>
#include <vector>
#include <memory>

using namespace std;

class Test // copyable and movable
{

};

class Test2 // only copyable
{
public:
    Test2(const Test2&) = default;
    Test2& operator=(const Test2&) = default;
};

class Shout
{
    int id_;
public:
    Shout(int id) : id_(id)
    {
        cout << "Ctor " << id_ << endl;
    }

    ~Shout()
    {
        cout << "Dtor " << id_ << endl;
    }

    Shout(const Shout& rop)
    {
        id_ = rop.id_;
        cout << "Copy Ctor " << rop.id_ << endl;
    }

    Shout operator=(const Shout& rop)
    {
        id_ = rop.id_;
        cout << "Copy Assignement " << rop.id_ << endl;
    }

    Shout(Shout&& rop) noexcept(noexcept(swap<int, int>)) // this allows to use it in vector reallocation
    {
        swap(id_, rop.id_);
        cout << "Move Ctor " << rop.id_ << endl;
    }

    Shout& operator=(Shout&& rop)  noexcept
    {
        id_ = rop.id_;
        cout << "Move Assignement " << rop.id_ << endl;
    }

    void fun()
    {
        cout << "Fun called " << id_ << endl;
    }
};

template<typename T>
class raii_ptr
{
    T* ptr_ = nullptr;
public:

    raii_ptr(const raii_ptr&) = delete;
    raii_ptr operator=(const raii_ptr&) = delete;

    raii_ptr(T* ptr) : ptr_(ptr)
    {

    }

    ~raii_ptr()
    {
        delete ptr_;
    }

    // move operators - TODO

    raii_ptr(raii_ptr&& rop) noexcept
    {
        cout << "Move ctor" << endl;
        swap(ptr_, rop.ptr_);
    }

    T* operator->() const
    {
        return ptr_;
    }

    T operator*() const
    {
        return *ptr_;
    }
};

raii_ptr<Shout> generate_shout(int n)
{
    return raii_ptr<Shout>(new Shout{n});
}

template<typename T, typename... Arg>
raii_ptr<T> make_raii_ptr(Arg&&... arg)
{
    return raii_ptr<T>(new T(std::forward<Arg...>(arg...)));
}

int main()
{
    cout << "Hello World!" << endl;
    //Shout* sht = new Shout(1);
    auto rptr = generate_shout(1);
    raii_ptr<Shout> rptr2(new Shout{2});
    rptr->fun();
    rptr2->fun();

    vector<raii_ptr<Shout>> vec;

    vec.push_back(generate_shout(3));

    vec.push_back(raii_ptr<Shout>{new Shout{4}});

    vec.emplace_back(new Shout(5));

    vec.push_back(make_raii_ptr<Shout>(6));

    for (auto& el : vec)
        el->fun();

    unique_ptr<Shout> ptr(new Shout(1));
    vector<unique_ptr<Shout>> vec2;
    vec2.emplace_back(new Shout(2));
    vec2.emplace_back(make_unique<Shout>(3));
}

