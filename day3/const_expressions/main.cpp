#include <iostream>
#include <array>

using namespace std;

constexpr int cube(int n)
{
    return n*n*n;
}

constexpr int factorial(int n)
{
    return (n == 0) ? 1 : n * (factorial(n-1));
}

template <typename T, size_t N>
constexpr size_t size_of_array(T (&)[N])
{
    return N;
}

int main()
{
    cout << "Hello World!" << endl;
    array<int, factorial(5)> arr;
    for (auto& el : arr)
        cout << el << endl;

    int tab1[10];
    int tab2[size_of_array(tab1)];

    int inp;
    cout << "give me a number" << endl;
    cin >> inp;
    cout << "cube = " << cube(inp) << endl;
    return 0;
}

