#include <iostream>
#include <vector>

using namespace std;

template <typename Cont>
void printc(Cont c)
{
    cout << "[ ";
    for (const auto& el : c)
    {
        cout << el << ", ";
    }

    cout << "]" << endl;
}

struct Point
{
    int x, y;
};

class Vec
{
    int x_, y_;
public:
    Vec(int x, int y) : x_(x), y_(y)
    {
        cout << "const with two parameters called" << endl;
    }

    Vec() : Vec(0, 0)
    {
        cout << "const without parameters called" << endl;
    }

    int x()
    {
        return x_;
    }

    int y()
    {
        return y_;
    }
};

class Items
{
public:
    Items(initializer_list<int> inlist)
    {
        cout << "Inlist initializer constructor" << endl;
        printc(inlist);
    }
};



int main()
{
    cout << "Hello Initialization" << endl;

    // C++98
    int i = 0;
    Vec v = Vec(10, 20);
    Point p;
    p.x = 10; p.y = 20;

    // C++11
    int i11{};
    Vec v11{};
    Point p11{10, 20};
    Point p11_2 = {10, 20};
    Vec v11_2 = {10, 20};

    vector<string> v_int{10,"a"};
    vector<int> v_int2(10,-1);
    vector<int> v_int3{10,-1};
    printc(v_int);
    printc(v_int2);
    printc(v_int3);

    //auto s = "Leszek"s + " "s + "Tarkowski"s;

    Items it{1,2,3,4,5};

    cout << v11_2.x() << endl;
    cout << i << endl;
    return 0;
}

