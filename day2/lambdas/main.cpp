#include <iostream>
#include <boost/type_index.hpp>
#include <vector>
#include <set>
#include <functional>

using namespace std;

class F
{
public:
    void operator()()
    {
        cout << "Functor f" << endl;
    }
};

class G
{
public:
    void operator()()
    {
        cout << "Functor g" << endl;
    }
};

class AddN
{
    int n;
public:
    AddN(int n) : n(n) {}
    int operator()(int m)
    {
        return n + m;
    }
};

//long operate_on(long n, long(*f)(long))
long operate_on(long n, std::function<long(long)> f)
{
    return f(n);
}

long sqr(long n)
{
    return n * n;
}

auto add_n(int n)
{
    return [&n](int m) { return m + n;};
}

int main()
{
    cout << "Hello lambdas!" << endl;

    int a = 123;

    auto f = + [] () -> void { cout << "Lambda f " << endl; }; // f is a function pointer
    auto g = [] () -> void { cout << "Lambda g " << endl; };   // g is lambda...
    f();
    g();

    F f2;
    f2();
    G g2;
    g2();
    void(*fptr)(void) = g;
    std::function<void()> fun_f = f;
    fptr();
    fun_f();
    cout << boost::typeindex::type_id_with_cvr<decltype(f)>().pretty_name() << endl;
    cout << boost::typeindex::type_id_with_cvr<decltype(g)>().pretty_name() << endl;

    // closure
    auto add5 = add_n(5);
    auto add25 = add_n(25);
    cout << "add5 + " << 10 << " = " << add5(10) << endl;
    cout << "add25 + " << 10 << " = " << add25(10) << endl;
    AddN add15(15);
    cout << "add15 + " << 10 << " = " << add15(10) << endl;

    // other captures

    vector<int> vec = {1,2,3,4,5,7,3,2,3};

    int counter{};

    auto count_number = [&vec, &counter] (int n)  { for(auto& el : vec) if (el == n)  ++counter;  };
    count_number(7);
    cout << "Res =" << counter << endl;


    // since C++14
    auto add = [] (auto a, auto b) { return a+b; }; // generalized lambdas

    cout << "Add 3+4 = " << add(3, 4) << endl;
    cout << "Add mam kota = "  << add("mam "s, "kota"s) << endl;

    auto cmp_ptrs = [](auto ptr1, auto ptr2) { return *ptr1 < *ptr2; };

    //set<int*, decltype(cmp_ptrs)> s{cmp_ptrs};
    set<int*, std::function<bool(int*, int*)>> s{cmp_ptrs};

    s.insert(new int(4));
    s.insert(new int(2));
    s.insert(new int(7));
    for (auto& el : s)
        cout << *el << endl;

    cout << "operate on = " << operate_on(5, sqr) << endl;
    cout << "operate on = " << operate_on(5, [] (long n) { return n*n;}) << endl;
    int x = 3;
    cout << "operate on = " << operate_on(5, [x] (long n) { return n*x;}) << endl;

    return 0;
}

