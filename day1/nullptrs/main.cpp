#include <iostream>
#include <memory>

using namespace std;

void f_ptr(int* ptr)
{
    if (ptr)
    {
        cout << "f(int* " << ptr << ")" << endl;
    }
    else
    {
        cout << "f(NULL)" << endl;
    }
}

void f_ptr(long a)
{
    cout << "f(long " << a << ")" << endl;
}

void f_ptr(nullptr_t)
{
    cout << "f(nullptr_t)" << endl;
}

int main()
{
    cout << "Hello nullptr" << endl;

    int a = 10;
    f_ptr(&a);
    f_ptr(NULL);
    f_ptr(static_cast<long>(NULL));
    //f_ptr(static_cast<long>(nullptr));
    f_ptr(nullptr);

    shared_ptr<int> sptr_i = nullptr;
    return 0;
}


