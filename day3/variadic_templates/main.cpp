#include <iostream>

using namespace std;

/* calling */

auto call()
{
    cout << "void fun" << endl;
}


//void call(int a)
//{
//    cout << "a = " << a << endl;
//}

auto call(int& a)
{
    cout << "a& = " << a << endl;
}

auto call(const int& a)
{
    cout << "const a& = " << a << endl;
}

auto call(int&& a)
{
    cout << "a&& = " << a << endl;
}

auto call(int a, int b) -> void
{
    cout << "a = " << a << " , b = " << b << endl;
}

template<typename... Args>
void fwd_call(Args&&... params)
{
    call(std::forward<Args>(params)...);
}

/* adder */

template<typename T>
T adder(T v)
{
    return v;
}

template<typename T, typename... Args>
T adder(T first, Args... args)
{
    return first + adder(args...);
}

/* count args */

template<typename... Args>
struct Count;

template<typename First, typename... Rest>
struct Count<First, Rest...>
{
    constexpr static int value = 1 + Count<Rest...>::value;
};

template<>
struct Count<>
{
    constexpr static int value = 0;
};


int main()
{
    cout << "Hello World!" << endl;
    // fwd_call(10, 20);

    int a = 3;
    int& b = a;
    const int& c = a;
    fwd_call(3); // rvalue
    fwd_call(a); // by value
    fwd_call(b); // by reference
    fwd_call(c); // by const reference
    //fwd_call();

    cout << adder(1,2,3,4,5) << endl;
    cout << adder(1,2,4,5) << endl;

    static_assert(Count<int, double, float>::value == 3, "must be 3");

    return 0;
}

