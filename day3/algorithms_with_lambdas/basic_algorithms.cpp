#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>
#include <random>

using namespace std;

int main()
{
    vector<int> vec(25);

    std::random_device rd;
    std::mt19937_64 mt {rd()};
    std::uniform_int_distribution<int> uniform_dist {1, 30};

    generate(vec.begin(), vec.end(), [&] { return uniform_dist(mt); } );

    for (auto& el : vec)
        el = uniform_dist(mt);

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    // 1a - wyświetl parzyste
    cout << "Parzyste : ";

    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, " "),
            [] (int val) { return !(val % 2);});

    cout << endl;

    // 1b - wyswietl ile jest parzystych
    cout << "ile parzystych? " << count_if(vec.begin(), vec.end(),
                                           [] (int val) { return !(val % 2);});
    cout << endl;

    int eliminators[] = { 3, 5, 7 };

    // 2 - usuń liczby podzielne przez dowolną liczbę z tablicy eliminators
    auto garbage_start = remove_if(vec.begin(), vec.end(),
                                   [&eliminators] (int elem) {
                                        return any_of(begin(eliminators), end(eliminators),
                                                      [elem](int elim) { return elem % elim == 0;});
                                   });
    vec.erase(garbage_start, vec.end());

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    // 3 - tranformacja: podnieś liczby do kwadratu
    // TODO

    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), vec.begin()+5, vec.end(), greater<int>());

    cout << "vec: ";
    for(const auto& item : vec)
        cout << item <<" ";
    cout << endl;

    // 5 - policz wartosc srednia
    cout << "Avg: " << accumulate(vec.begin(), vec.end(), 0.0)/vec.size() << endl;

    auto sum = 0.0;
    auto max = 0;
    for_each(vec.begin(), vec.end(), [&](int elem) { sum += elem; if (elem > max) max = elem; });
    cout << "Avg: " << sum/vec.size() << endl;
    cout << "Max: " << max << endl;


    // 6 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej, 2. z liczbami większymi od średniej
    // TODO
}
