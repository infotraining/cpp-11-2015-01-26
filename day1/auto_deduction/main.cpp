#include <iostream>
#include <map>
#include <vector>
#include <string>
#include <boost/type_index.hpp>

using namespace std;

template <typename T>
void deduce(T t);

double res()
{
    return 3.14;
}

int main()
{
    cout << "Hello C++11" << endl;

    // deduction of types
    auto i = 42;

    const int a = 42;
    auto b = a;
    //deduce(b);

    const int& c = a;
    auto d = c;

    const auto& e = c;

    int tab1[3] = {1,2,3};

    auto tab2 = tab1;
    //deduce(tab2);
    cout << boost::typeindex::type_id_with_cvr<decltype(d)>().pretty_name() << endl;

    //for(auto& i : tab2)
    //    cout << i << endl; // does not work for deduced type

    map<string, int> m;
    m["ala"] = 10;
    m["ola"] = 20;


    for(auto it = begin(m); it != end(m); ++it)
    {
        cout << it->first << endl;
        it->second = 100;
    }

    cout << "range based" << endl;

    for(auto& elem : m)
    {
        //cout << elem.first << " : " << elem.second << endl;
        elem.second = 200;
    }

    //for(pair<const string, int>& elem : m)
    for(auto& elem : m)
    {
        //cout << elem.first << " : " << elem.second << endl;
        elem.second = 200;
    }

    for(const auto& elem : m)
    {
        cout << elem.first << " : " << elem.second << endl;
    }

    // initializer lists...

    int list1[] = {1,2,3};

    for (const auto& el : list1)
        cout << el << endl;

    auto list2 = {4,5,6};

    for (const auto& el : list2)
        cout << el << endl;

    cout << boost::typeindex::type_id_with_cvr<decltype(list2)>().pretty_name() << endl;


    auto r = static_cast<int>(res()); //preferred way of explicite conversion
    int r2 = res();

    cout << "Result = " << r << endl;

    vector<bool> bitvec {0,1,1,0};

    for (auto&& el : bitvec)
    {
        el.flip();
    }

    for (auto&& el : bitvec)
    {
        cout << el << endl;
    }

    return 0;
}

