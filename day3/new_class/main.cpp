#include <iostream>
#include <memory>

using namespace std;

class Base {
public:
    virtual void doWork() final
    {
        cout << "Base::doWork" << endl;
    }

    virtual void mf1() const
    {
        cout << "Base::mf1" << endl;
    }

    virtual void mf2(const int& a)
    {
        cout << "Base::mf2" << endl;
    }
};

class Derived : public Base
{
public:
//    virtual void doWork()
//    {
//        cout << "Derived::doWork" << endl;
//    }

    void mf1() const override
    {
        cout << "Derived::mf1" << endl;
    }

    void mf2(const int& a) override
    {
        cout << "Derived::mf1" << endl;
        Base::doWork();
    }
};

int main()
{
    std::unique_ptr<Base> upb = make_unique<Derived>();
    upb->doWork();
    upb->mf1();
    upb->mf2(12);
    return 0;
}

