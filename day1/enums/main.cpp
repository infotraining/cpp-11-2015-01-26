#include <iostream>

using namespace std;

enum Coffee : unsigned char { espresso, cappucino, latte };
enum Tea : unsigned char { leaf, green };

enum class Engine : char { diesel = 1, gasoline, electric };
enum class Wheels : char { rubber = 1, silicon, steel };

int main()
{
    Coffee my_coffee = espresso;
    Tea my_tea = leaf;
    if (my_coffee == my_tea)
    {
        cout << "The same breverage" << endl;
    }
    int val = cappucino + latte;
    cout << "Value of espresso " << val << endl;

    // C++11 - new enums
    Engine e = Engine::diesel;
    // int val2 = Engine::diesel; does not work!!
    int val2 = static_cast<int>(Engine::electric);
    cout << "Value of electric = " << val2 << endl;

    Wheels w = Wheels::rubber;

    /* if (w == e)
    {
        cout << "The same part" << endl;
    } */  //cannot compare Wheels and Engines

    if (e == Engine::electric)
        cout << "Engine = electric" << endl;

    switch(e)
    {
        case Engine::diesel:
            cout << "diesel" << endl;
            break;
        case Engine::electric:
            cout << "electric" << endl;
            break;
        default:
            cout << "some engine" << endl;
    }


    return 0;
}

